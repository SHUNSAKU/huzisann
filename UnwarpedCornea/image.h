//
//  image.h
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/10/02.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#ifndef __UnwarpedCornea__image__
#define __UnwarpedCornea__image__

#include <stdio.h>
#include <opencv2/opencv.hpp>
#endif /* defined(__UnwarpedCornea__image__) */


void ProjectedIrisArea(cv::Mat &frame,std::vector<cv::Mat> &iris3, cv::Point (&iris_pt)[72]);
double EvaluatedIrisArea(double pitch, double yaw, std::vector<cv::Mat> &iris3_pos,cv::Mat &im,cv::Mat &bin,cv::Mat &frame);
void ProjectedIrisArea(cv::Mat &frame, cv::Mat &point);
cv::Mat GenerateUnwarpedImage(cv::Mat &Point, cv::Mat &org);
void UnwarpingImage(cv::Mat img4unwarp, cv::Mat LrefPos);