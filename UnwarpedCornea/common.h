//
//  common.h
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/10/31.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#ifndef UnwarpedCornea_common_h
#define UnwarpedCornea_common_h

#define WIDTH 320
#define HEIGHT 320
#define SCALE 2 //展開用接平面のサイズを決めるもの(偶数の方がよいと思う)


//#define CELLING
#define LIVE
//#define SCENE
//#define DESKTOP
#define UNWARPED
//#define WHEEL_CHAIR
//#define SOCKET
//#define BRUTE_FORCE
#endif
