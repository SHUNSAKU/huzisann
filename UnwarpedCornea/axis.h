//
//  axis.h
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/08/19.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#ifndef __UnwarpedCornea__axis__
#define __UnwarpedCornea__axis__

#include <iostream>
#include <GLFW/glfw3.h>
#include <opencv2/opencv.hpp>
void drawGridPlaneXY();
void drawGridPlaneXZ();
void drawAxisXYZ(double *pos);
void drawSphere(double *pos, double radius, double *color);
void drawImagePlane(double *color, double width, double height, double *pos  );
void showVec(double *start, double *goal,double *color);
GLFWwindow* initalize();
void drawTangentPlane(cv::Mat &plane);
#endif /* defined(__UnwarpedCornea__axis__) */
