//
//  sharedmem.h
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/11/29.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#ifndef __UnwarpedCornea__sharedmem__
#define __UnwarpedCornea__sharedmem__

#include <iostream>

#define SHM_KEY 2000

char* InitSharedMem(int key, int size);
void send_parameters(char *shm, float yaw_angle, float pitch_angle);

/*! 通信:共有メモリに書かれるデータ */
typedef struct{
    float yaw_angle;
    float pitch_angle;

} SendData;

#endif /* defined(__UnwarpedCornea__sharedmem__) */
