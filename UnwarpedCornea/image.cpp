//
//  image.cpp
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/10/02.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#include "common.h" 
#include "image.h"
#include "eyeball.h"

extern double D;
extern cv::Mat NI;
extern cv::Mat P0;
extern cv::Mat unwarp;

extern int cx;
extern int cy;
extern double MMPERPIXEL;

extern int Unwarped_flg;
extern int UnwarpedImg_flg;

void ProjectedIrisArea(cv::Mat &frame,std::vector<cv::Mat> &iris3, cv::Point (&iris_pt)[72]){
    std::vector<cv::Mat>::iterator iris3_posi=iris3.begin();
    std::vector<cv::Mat> iris2;
    cv::Mat iris2_tmp;
    while (iris3_posi != iris3.end()) {
        double t = - D/NI.dot(*iris3_posi/norm(*iris3_posi));
        cv::Mat tmp= t*(*iris3_posi/norm(*iris3_posi));
        iris2_tmp= (cv::Mat_<double>(2,1) << tmp.at<double>(0)/MMPERPIXEL+cx,tmp.at<double>(1)/MMPERPIXEL+cy);
        iris2.push_back(iris2_tmp);
        iris3_posi++;
    }
    
    std::vector<cv::Mat>::iterator iris2i = iris2.begin();
    //    cv::Point iris_pt[72];// 72 = 359/5
    int pt_num=0;
    while (iris2i != iris2.end()) {
        cv::Mat tmp = *iris2i;
        cv::circle(frame, cv::Point(tmp.at<double>(0),tmp.at<double>(1)), 2, cv::Scalar(255,255,0));
        iris_pt[pt_num]=cv::Point(tmp.at<double>(0),tmp.at<double>(1));
        //                cout <<*iris2i<<endl;
        pt_num++;
        iris2i++;
    }
}

void ProjectedIrisArea(cv::Mat &frame, cv::Mat &point){
        double t = - D/NI.dot(point/norm(point));
        cv::Mat tmp= t*(point/norm(point));
        cv::Mat iris2_tmp= (cv::Mat_<double>(2,1) << tmp.at<double>(0)/MMPERPIXEL+cx,tmp.at<double>(1)/MMPERPIXEL+cy);
        cv::circle(frame, cv::Point(iris2_tmp.at<double>(0),iris2_tmp.at<double>(1)), 5, cv::Scalar(0,255,255));
#ifdef DEBUG
//    std::cout <<"iris2_tmp="<<iris2_tmp << std::endl;
#endif

}


double EvaluatedIrisArea(double pitch, double yaw, std::vector<cv::Mat> &iris3_pos,cv::Mat &im,cv::Mat &bin,cv::Mat&frame){
    im.setTo(0);
    cv::Point iris_pt[72];
    cv::Mat and_im;
    std::vector<cv::Mat> simulated_irispos=rotateEyeModel(pitch,yaw,iris3_pos);
    ProjectedIrisArea(frame,simulated_irispos,iris_pt);
    cv::fillConvexPoly(im,iris_pt,72, cv::Scalar(255));
    cv::bitwise_and(im, bin,and_im);
    
    return cv::sum(and_im).val[0];
}

cv::Mat GenerateUnwarpedImage(cv::Mat &Point,cv::Mat &org){
    cv::Mat image(HEIGHT+1,WIDTH+1,CV_8UC3);
    for (int y=0; y<HEIGHT+1; y++ ) {
        for (int x=0; x<WIDTH+1; x++) {
//            std::cout << Point.at<cv::Vec3d>(y,x)[1] <<" "<< int(Point.at<cv::Vec3d>(y,x)[0]) << std::endl;
//            std::cout << int(Point.at<cv::Vec3d>(y,x)[1]/MMPERPIXEL+cy) <<" "<< int(Point.at<cv::Vec3d>(y,x)[0]/MMPERPIXEL+cx) << std::endl;

            image.at<cv::Vec3b>(y,x)[0]=org.at<cv::Vec3b>(int(Point.at<cv::Vec3d>(y,x)[1]/MMPERPIXEL+cy),int(Point.at<cv::Vec3d>(y,x)[0]/MMPERPIXEL+cx))[0];
            image.at<cv::Vec3b>(y,x)[1]=org.at<cv::Vec3b>(int(Point.at<cv::Vec3d>(y,x)[1]/MMPERPIXEL+cy),int(Point.at<cv::Vec3d>(y,x)[0]/MMPERPIXEL+cx))[1];
            image.at<cv::Vec3b>(y,x)[2]=org.at<cv::Vec3b>(int(Point.at<cv::Vec3d>(y,x)[1]/MMPERPIXEL+cy),int(Point.at<cv::Vec3d>(y,x)[0]/MMPERPIXEL+cx))[2];
        }
    }
    return image;
}

void UnwarpingImage(cv::Mat img4unwarp, cv::Mat LrefPos){
    std::cout << "-----------------Unwarping START------------------------"<<std::endl;
    
    //Define the tangent plane. 接平面の設定
    cv::Mat plane=SetPlane(LrefPos);
    
    //Define the vertical plane 垂直平面の設定
#ifdef CELLING
    cv::Mat rvec3(3,1,CV_64FC1);
    cv::Mat goal=(cv::Mat_<double>(3,1) << plane.at<cv::Vec3d>(0,0)[0],plane.at<cv::Vec3d>(0,0)[1],plane.at<cv::Vec3d>(0,0)[2]);
    cv::Mat start= (cv::Mat_<double>(3,1) << plane.at<cv::Vec3d>(0,plane.cols-1)[0],plane.at<cv::Vec3d>(0,plane.cols-1)[1],plane.at<cv::Vec3d>(0,plane.cols-1)[2]);
    rvec3= goal-start;
    rvec3=M_PI*(80.0/180.0)*(rvec3/cv::norm(rvec3));
    
#ifdef DEBUG
    cout << "rvec3=" << rvec3 << endl;
#endif
    
    cv::Mat RotPlane(3,3,CV_64FC1);
    cv::Rodrigues(rvec3,RotPlane);
    cv::Mat plane2=plane.clone();
    
    for (int n=0; n < plane.rows; n++) {
        for (int m=0; m<plane.cols; m++) {
            cv::Mat pixel=(cv::Mat_<double>(3,1) << plane.at<cv::Vec3d>(n,m)[0],plane.at<cv::Vec3d>(n,m)[1],plane.at<cv::Vec3d>(n,m)[2]);
            cv::Mat plane2tmp=RotPlane*(pixel-goal)+goal;
            plane2.at<cv::Vec3d>(n,m)[0]=plane2tmp.at<double>(0);
            plane2.at<cv::Vec3d>(n,m)[1]=plane2tmp.at<double>(1);
            plane2.at<cv::Vec3d>(n,m)[2]=plane2tmp.at<double>(2);
        }
    }
    plane2.copyTo(plane);
#endif
    
    cv::Mat Point=CalcurateReflectionVec(plane);        // Relationship between tangent plane and image plane. 接平面と画像平面の対応関係
    unwarp=GenerateUnwarpedImage(Point, img4unwarp);    // Generating Unwarped Image. 画像生成
    
    Unwarped_flg=true;
    UnwarpedImg_flg=true;
    
}
