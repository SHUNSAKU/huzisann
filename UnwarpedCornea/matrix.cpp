//
//  matrix.cpp
//  UnwarpedCornea
//
//  Created by 植田　俊作 on 2017/06/26.
//  Copyright © 2017年 ___Kentaro Takemura___. All rights reserved.
//

#include "matrix.hpp"
#include <opencv2/opencv.hpp>
#include <boost/asio.hpp>
#include <GLFW/glfw3.h>
#include <OpenGL/glu.h>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include <iomanip>
#include <ctype.h>

#include "axis.h"
#include "eyeball.h"
#include "geometry.h"
#include "image.h"
#include "equation.h"
#include "common.h"
#include "sharedmem.h"
#include "eyemodel.h"
#include "eyemodel.h"

//Camera Paramters;
extern double MMPERPIXEL;
extern double focus;
extern int cx;
extern int cy;


//入力画像平面
extern cv::Mat NI;
extern cv::Mat P0;
extern double D;

//Eye Paramters;
extern double rl;
extern double dlc;
extern double rc;
extern double rcl;
extern double re;

//Eye Model;
extern double d;
extern cv::Mat L;
extern cv::Mat g;
extern cv::Mat C;
extern cv::Mat CC; //眼球中心
extern cv::Mat L2;
extern cv::Mat S ;
//cv::Mat LrefPos;

void t_matrix(){
    double t_matrix ;
    
    
   // P=RcN/|N|
    

}
