//
//  geometry.cpp
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/08/20.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//
#include <opencv2/opencv.hpp>
#include <iostream>
#include "geometry.h"


std::vector<cv::Mat> CalcIntersectionSphere2Sphere(cv::Mat &C,double rc, cv::Mat &CC, double re){

#if 0
    cv::Mat O=cv::Mat::zeros(3,1,CV_64F);
//    cv::Mat NewC=C-CC;//角膜方向ベクトル
    cv::Mat NewC = (cv::Mat_<double>(3,1,CV_64F) << 0,0,-norm(C-CC));
    cv::Mat NewO=O-CC;//原点方向ベクトル
    
    cv::Mat RVec=NewO.cross(NewC)/norm(NewO.cross(NewC)); // rotation axis
    double Rangle = acos(NewO.dot(NewC)/(norm(NewO)*norm(NewC)));
    cv::Mat CPMatrix = (cv::Mat_<double>(3,3,CV_64F) << 0,-RVec.at<double>(2),RVec.at<double>(1),RVec.at<double>(2),0,-RVec.at<double>(0),-RVec.at<double>(1),RVec.at<double>(0),0);//Cross Prduct Matrix
    cv::Mat RMatrix = cos(Rangle) * cv::Mat::eye(3,3,CV_64F)+(1-cos(Rangle))*RVec*RVec.t()+sin(Rangle)*CPMatrix;



#ifdef DEBUG
    std::cout <<"RVec=" <<RVec << std::endl;
    std::cout <<"Rangle=" <<Rangle << std::endl;
    std::cout <<"CPMatrix=" <<CPMatrix<< std::endl;
    std::cout <<"RMatrix=" <<RMatrix<< std::endl;
#endif
    
    double tmp= -norm(NewC);

    double a= 1/(2*tmp)*sqrt(4*tmp*tmp*re*re-pow((tmp*tmp-rc*rc+re*re),2));
    
    std::vector<cv::Mat> iris3; //3D Points of Intersection between sphere and sphere
    for (int theta=0 ; theta<359; theta=theta+5){
        cv::Mat iris_p = (cv::Mat_<double>(3,1)<< a*sin(theta/180.0*M_PI), a*cos(theta/180.0*M_PI),(tmp*tmp-rc*rc+re*re)/(2*tmp));
        cv::Mat iris_tmp=RMatrix * iris_p +CC;
        iris3.push_back(iris_tmp);
//        std::cout <<"x="<<iris_tmp.at<double>(0) <<"y="<<iris_tmp.at<double>(1)<<"z="<<iris_tmp.at<double>(2)<<std::endl;
    }
    
#else
    cv::Mat O=cv::Mat::zeros(3,1,CV_64F);
    cv::Mat NewC = (cv::Mat_<double>(3,1,CV_64F) << 0,0,-norm(C-CC));
    cv::Mat NewCC =(cv::Mat_<double>(3,1,CV_64F) << 0,0,0);
    cv::Mat DirC=C-CC;//原点方向ベクトル
    
    cv::Mat RVec=NewC.cross(DirC)/norm(NewC.cross(DirC)); // rotation axis
    double Rangle = acos(DirC.dot(NewC)/(norm(DirC)*norm(NewC)));
    cv::Mat CPMatrix = (cv::Mat_<double>(3,3,CV_64F) << 0,-RVec.at<double>(2),RVec.at<double>(1),RVec.at<double>(2),0,-RVec.at<double>(0),-RVec.at<double>(1),RVec.at<double>(0),0);//Cross Prduct Matrix
    cv::Mat RMatrix = cos(Rangle) * cv::Mat::eye(3,3,CV_64F)+(1-cos(Rangle))*RVec*RVec.t()+sin(Rangle)*CPMatrix;
    
    
    
#ifdef DEBUG
    std::cout <<"RVec=" <<RVec << std::endl;
    std::cout <<"Rangle=" <<Rangle << std::endl;
    std::cout <<"CPMatrix=" <<CPMatrix<< std::endl;
    std::cout <<"RMatrix=" <<RMatrix<< std::endl;
#endif
    
    double tmp= -norm(NewC);
    
    double a= 1/(2*tmp)*sqrt(4*tmp*tmp*re*re-pow((tmp*tmp-rc*rc+re*re),2));
    
    std::vector<cv::Mat> iris3; //3D Points of Intersection between sphere and sphere
    for (int theta=0 ; theta<359; theta=theta+5){
        cv::Mat iris_p = (cv::Mat_<double>(3,1)<< a*sin(theta/180.0*M_PI), a*cos(theta/180.0*M_PI),(tmp*tmp-rc*rc+re*re)/(2*tmp));
        cv::Mat iris_tmp=RMatrix * iris_p +CC;
        iris3.push_back(iris_tmp);
        //        std::cout <<"x="<<iris_tmp.at<double>(0) <<"y="<<iris_tmp.at<double>(1)<<"z="<<iris_tmp.at<double>(2)<<std::endl;
    }
#endif
    return iris3;
}

cv::Mat IntersectionRay2Sphere(cv::Mat &Ray, cv::Mat &Center, double radius){
    double b = -2 * Ray.dot(Center);//Rayがillummination
    cv::Mat c= Center.t()*Center-radius*radius;
    cv::Mat D= b * b -4*(Ray.t()*Ray)*c.at<double>(0);
    cv::Mat t= (-b-sqrt(D.at<double>(0)))/(2*Ray.t()*Ray);
    cv::Mat IntersectionPoint = t.at<double>(0)*Ray;
    return IntersectionPoint;
}

