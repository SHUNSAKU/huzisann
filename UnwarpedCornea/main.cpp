//
//  main.cpp
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/07/07.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#include <opencv2/opencv.hpp>
#include <boost/asio.hpp>
#include <GLFW/glfw3.h>
#include <OpenGL/glu.h>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include <iomanip>
#include <ctype.h>
#include <ApplicationServices/ApplicationServices.h>


#include "axis.h"
#include "eyeball.h"
#include "geometry.h"
#include "image.h"
#include "equation.h"
#include "common.h"
#include "sharedmem.h"
#include "eyemodel.h"

using namespace std;

//cv::VideoCapture cap(0);  // VideoCapture(int device) for eye
cv::Mat test_image=cv::imread("/Users/ueda.s/Desktop/test.png",cv::IMREAD_UNCHANGED);

//#ifdef SCENE
//cv::VideoCapture cap1(1);  // VideoCapture(int device) for scene
//#endif

#define FLIP_ON 1

//thread
std::mutex mtx1,mtx2;
int Unwarped_flg=true;
int UnwarpedImg_flg=false;
int showModel_flg=false;

string filename;
int number=24;
cv::Mat frame, img, org,bin_im,gray_im, scene, result;
std::vector<cv::Point2f> iris;
std::vector<cv::Point2f> corner;
std::vector<cv::Point2f> illumination;
std::vector<cv::Point3f> illumination_3;
std::vector<cv::Mat> nv1;
std::vector<cv::Mat> intersection;
cv::Point iris_pt[72];
std::vector<cv::Mat> iris3_pos;
cv::Mat LrefPos;
double max_pitch;
double max_yaw;
std::vector<cv::Mat>Matrix_test;

int phase=0;

//Camera Paramters;
double MMPERPIXEL;
double focus;
int cx;
int cy;

GLFWwindow* glwindow;

extern double d;
extern cv::Mat eyeY;
extern cv::Mat eyeX;
extern cv::Mat eyeZ;

//入力画像平面
cv::Mat NI;
cv::Mat P0;
double D;

extern cv::Mat C;
extern double rc;


//GUI
int value=30;

//Shared Mem
char *shm;

cv::Mat unwarp;

void set_cam_paramter(){
    MMPERPIXEL=0.00387384;
    focus = MMPERPIXEL * 2404.8;
    cx=320;
    cy=240;
}




void onMouse( int event, int x, int y, int flag, void* )
{
    std::string desc;
    
    // マウスイベントを取得
    switch(event) {
        case cv::EVENT_MOUSEMOVE:
            break;
        case cv::EVENT_LBUTTONDOWN:
            break;
        case cv::EVENT_RBUTTONDOWN:
            break;
        case cv::EVENT_MBUTTONDOWN:
            break;
        case cv::EVENT_LBUTTONUP:
            desc += "LBUTTON_UP";
            if(phase==0){
                cv::circle(img, cv::Point(x,y), 2, cv::Scalar(0,255,0));
                iris.push_back(cv::Point(x,y));
            }
            else if(phase==1){
                cv::circle(img, cv::Point(x,y), 2, cv::Scalar(0,0,255));
                corner.push_back(cv::Point(x,y));
            }
            else if(phase==2){
                cv::circle(img, cv::Point(x,y), 2, cv::Scalar(255,0,0));
                illumination.push_back(cv::Point(x,y));

                
            }
            
        cv::imshow("Camera", img);
        break;
        case cv::EVENT_RBUTTONUP:
            
            
            
            break;
        case cv::EVENT_MBUTTONUP:
            break;
        case cv::EVENT_LBUTTONDBLCLK:
            break;
        case cv::EVENT_RBUTTONDBLCLK:
            break;
        case cv::EVENT_MBUTTONDBLCLK:
            break;
    }
    
#ifdef DEBUG
    //    std::coose-Invariant Gaze Estimation by Tracking Iris and Eyelidt << desc << " (" << x << ", " << y << ")" << std::endl;
#endif
    
}

//# コンパイルされる前に通るもの



int main (int argc, const char **argv) {
    
    cv::namedWindow("Camera", CV_WINDOW_AUTOSIZE);
    cv::namedWindow("Processing",CV_WINDOW_AUTOSIZE);
//    cv::namedWindow("test",CV_WINDOW_AUTOSIZE);
    
//#ifdef SCENE
//    cv::namedWindow("Scene",CV_WINDOW_AUTOSIZE);
//#endif
//#ifdef DESKTOP
//    cv::namedWindow("Desktop",CV_WINDOW_AUTOSIZE);
//#endif
    
    cv::setMouseCallback("Camera", onMouse, 0);//callback
    cv::createTrackbar("track", "Processing", &value, 255);
    
    //Initalize for shared memory
    shm = InitSharedMem(SHM_KEY, sizeof(SendData));
    
    
    //Setting paramters
    set_cam_paramter();
    set_eye_paramter();
    
    //read image
#ifdef LIVE
//    if(!cap.isOpened()) return -1;//もしカメラが起動されていなければ
#ifdef SCENE
    if(!cap1.isOpened()) return -1;
#endif
#ifdef DESKTOP
    size_t width = CGDisplayPixelsWide(CGMainDisplayID());
    size_t height = CGDisplayPixelsHigh(CGMainDisplayID());
    
    
    cv::Mat image(cv::Size(width,height),CV_8UC4,cv::Scalar(0,0,0));
    cv::Mat bgrimage(cv::Size(width, height), CV_8UC3, cv::Scalar(0,0,0));
    cv::Mat resizeimage(cv::Size(width/2, height/2), CV_8UC3, cv::Scalar(0,0,0));
    /* グラフィックコンテキストの作成 */
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef contextRef = CGBitmapContextCreate(image.data, width, height, 8,image.step[0], colorSpace, kCGImageAlphaPremultipliedLast|kCGBitmapByteOrderDefault);
#endif
    int key;
    
    cout << "Please press N key for calibration" << endl;
    
    while (key!='n') {
//        cap >> frame;
        frame = test_image;
        

//#ifdef SCENE
//        cap1 >> scene;
//#endif
//#ifdef DESKTOP
//        /* メインディスプレイからCGImageRefを取得 */
//        CGImageRef imageRef = CGDisplayCreateImage(CGMainDisplayID());
//        CGContextDrawImage(contextRef,CGRectMake(0, 0, width, height),imageRef);
//        
//        /* RGBAからBGRに変換して，リサイズ */
//        cv::cvtColor(image, bgrimage, CV_RGBA2BGR);
//        cv::resize(bgrimage, resizeimage,resizeimage.size());
//#endif
//        
        cv::flip(frame, img, FLIP_ON);
        value = cv::getTrackbarPos("track", "Processing");
        
        cv::cvtColor(img, gray_im, CV_RGB2GRAY);
        cv::threshold(gray_im, bin_im, value, 255, cv::THRESH_BINARY_INV);//現在のところしきい値は手動で設定.　また白黒反転
        cv::imshow("Processing", bin_im);
        imshow("Camera",img);
        
//#ifdef SCENE
//        imshow("Scene",scene);
//#endif
//#ifdef DESKTOP
//        imshow("Desktop",resizeimage);
//        CGImageRelease(imageRef);
//#endif
        key=cv::waitKey(30);//でバックする時に便利　キーが押されるまでwhile文
    }
    
    org = frame;
    
    
#else
    std::ostringstream os;
    //    os <<"./Image/img"<<setw(10)<<setfill('0')<<number<<".jpg";
    os <<"300.jpg";
    filename=os.str();
    frame = cv::imread(filename);
    frame.copyTo(img);
    
#endif
    
    img.copyTo(org);//ingをorgにコピー
    cv::imshow("Camera", img);
    cout << "Please click limbus, and push Enter key" << endl;
    cv::waitKey(-1);
    
    phase=1;
    //fitting
    cv::RotatedRect box=cv::fitEllipse(cv::Mat(iris));//点群に剃って一番綺麗な楕円が描写
    cv::ellipse(img, box,cv::Scalar(255,0,0),2,8);//opencvはk基本BGR
    cv::circle(img, box.center, 2, cv::Scalar(0,0,255));
    
    
    cout << "Please click eye corner，and push Enter Key" << endl;
    cv::imshow("Camera", img);
    cv::waitKey(-1);
    
    phase=2;
    cv::circle(img, box.center, 2, cv::Scalar(255,0,0));
    cout << "Please click illuminations" << endl ;
    cv::imshow("Camera",img);
    cv::waitKey(-1);
    

    //照明の座標の変換2→3次元
    std::vector<cv::Point2f>transformation(3);
    for(int i = 0;i<3; i++){
    transformation[i].x = (float)MMPERPIXEL* (illumination[i].x-cx);
    transformation[i].y = (float)MMPERPIXEL* (illumination[i].y-cy);
    //illumination_3.x = MMPERPIXEL * illumination[0].x;
    illumination_3.push_back(cv::Point3f(transformation[i].x,transformation[i].y,focus));
    }
    cout<<"2dillumination"<<endl<<illumination<<endl;
    cout<<"3dillumination"<<endl<<illumination_3<<endl;
    
    
  
    
    
    CreateEyeModel(box);
    CalcEyeAxis(corner.at(0),corner.at(1));
    iris3_pos=CalcLimbus();

    //RAYの衝突点の計算
    for(int i=0 ; i<3 ; i++){
        cv::Mat Light = (cv::Mat)illumination_3[i];
    cv::Mat Light2 = (cv::Mat_<double>(3,1) << illumination_3[i].x,illumination_3[i].y,illumination_3[i].z);
    intersection.push_back(IntersectionRay2Sphere(Light2, C, rc));
        std::cout<<"intersection"<<intersection[i]<<endl;
        
        //法線ベクトルの決定
        double norm_n1 = cv::norm(intersection[i] - C);//.at<duble>[1][3]
        std::cout<<"norm1"<<norm_n1<<std::endl;
        nv1.push_back((intersection[i]-C)/norm_n1); //Mat型、後からVecなどに直す必要ありnv1+cc
        }
    
    
   
    
//    //法線ベクトルの決定
//    double norm_n1 = cv::norm(intersection - C);//.at<duble>[1][3]
//    std::cout<<"norm1"<<norm_n1<<std::endl;
//    nv1.push_back((intersection-C)/norm_n1); //Mat型、後からVecなどに直す必要ありnv1+cc
    
    //変換行列Mの確認
    
    //Definition of input image plane.入力画像平面の定義
    NI = (cv::Mat_<double>(3,1) << 0,0,1);
    P0 = (cv::Mat_<double>(3,1) << MMPERPIXEL* (-cx), MMPERPIXEL*(-cy), focus);
    D = -NI.dot(P0);
    
    //Projection of model's iris info. モデルの虹彩情報を投影
    ProjectedIrisArea(img,iris3_pos, iris_pt);
    cout << "Show Estimated Iris Area" << endl;
    cv::imshow("Camera", img);
    cv::waitKey(-1);
    
    
    //thread
    //    boost::thread thr_tracking(&tracking);
    
#ifdef UNWARPED
    //    boost::thread thr_unwarping(&unwarping);
#endif
    
    cv::Mat tmp;
    cv::Mat simulated_im(img.size(),CV_8UC1,cv::Scalar(0));
    double max_value;
    
    double max_value1;
    double max_pitch1;
    double max_yaw1;
    double max_value2;
    double max_pitch2;
    double max_yaw2;
    double pitch = -10, yaw=-30;//Inital value
    double search = true;//
    
    double value1;
    double value2;
    double value3;
    
    std::vector<cv::Mat> estimated_irispos;
    
    
#ifdef LIVE
    while (1) {
        //キャプチャーはthread内に記述してはいけない．
        frame = test_image;
        
//#ifdef SCENE
//        cap1 >> scene;
//#endif
//#ifdef DESKTOP
//        /* メインディスプレイからCGImageRefを取得 */
//        CGImageRef imageRef = CGDisplayCreateImage(CGMainDisplayID());
//        CGContextDrawImage(contextRef,CGRectMake(0, 0, width, height),imageRef);
//        
//        /* RGBAからBGRに変換して，リサイズ */
//        cv::cvtColor(image, bgrimage, CV_RGBA2BGR);
//        cv::resize(bgrimage, resizeimage,resizeimage.size());
//#endif
        
        
        
//        cv::flip(frame, img,FLIP_ON);
        img = frame ;
        img.copyTo(org);
        // Iris tracking
        
        cv::cvtColor(org, gray_im, CV_RGB2GRAY);
        cv::threshold(gray_im, bin_im, value, 255, cv::THRESH_BINARY_INV);//現在のところしきい値は手動で設定.　また白黒反転
        
        //Rotate eye
        
        max_value=0;
        max_pitch=0;
        max_yaw=0;
        
        max_value1=0;
        max_pitch1=0;
        max_yaw1=0;
        max_value2=0;
        max_pitch2=0;
        max_yaw2=0;
        
        img.copyTo(tmp);
        
#ifdef BRUTE_FORCE
        for (double pitch=-10; pitch<10; pitch=pitch+1) {
            for (double yaw=-30; yaw<30; yaw=yaw+1) {
                
                tmp.copyTo(img);//表示用にコピー
                
                double t getTrackbarPos(const string& trackbarname, const string& winname)=EvaluatedIrisArea(pitch, yaw, iris3_pos, simulated_im, bin_im,img);
                
                if(value > max_value){
                    max_value = value;
                    max_pitch = pitch;
                    max_yaw =    yaw;
                    //                cout <<"value= "<<value<<" "<<"pitch=" << max_pitch <<" "<<"yaw=" << max_yaw << endl;
                }
                
                cv::imshow("Camera", img);
                //                cv::imshow("Processing", and_im);
                cv::waitKey(30);
            }
        }
#else
        //Hill climbing algorithm
        pitch = -10;
        yaw=-30;//Inital value
        search = true;//
        while (search) {
            tmp.copyTo(img);//表示用にコピー
            value1=EvaluatedIrisArea(pitch+1, yaw, iris3_pos, simulated_im, bin_im,img);//pitch direction
            value2=EvaluatedIrisArea(pitch, yaw+1, iris3_pos, simulated_im, bin_im,img);//yaw direction
            value3=EvaluatedIrisArea(pitch+1, yaw+1, iris3_pos, simulated_im, bin_im,img);//pitch,and yaw direction
            
            if (value3 >= max_value1 && value3 > value2 && value3 > value1) {
                //            cout << "value3" << endl;
                max_value1 = value3;
                max_pitch1 = pitch;
                max_yaw1 = yaw;
                
                pitch=pitch+1;
                yaw=yaw+1;
            }
            else if(value2 > max_value1 && value2 > value3 && value2 > value1){
                //            cout << "value2" << endl;
                max_value1 = value2;
                
                max_pitch1 = pitch;
                max_yaw1 = yaw;
                
                pitch=pitch;
                yaw=yaw+1;
                
            }
            else if(value1 > max_value1 && value1 > value3 && value1 > value2){
                //            cout << "value1" << endl;
                max_value1 = value1;
                
                max_pitch1 = pitch;
                max_yaw1 = yaw;
                
                pitch=pitch+1;
                yaw=yaw;
            }
            else{
                search = false; //ループ終了
            }
            //        cv::imshow("Camera", img);
            //        cv::waitKey(30);
        }
        
        pitch = 10, yaw=30;//Initial value
        search = true;//
        while (search) {
            tmp.copyTo(img);//表示用にコピー
            value1=EvaluatedIrisArea(pitch-1, yaw, iris3_pos, simulated_im, bin_im,img);//pitch direction
            value2=EvaluatedIrisArea(pitch, yaw-1, iris3_pos, simulated_im, bin_im,img);//yaw direction
            value3=EvaluatedIrisArea(pitch-1, yaw-1, iris3_pos, simulated_im, bin_im,img);//pitch,and yaw direction
            
            if (value3 >= max_value2 && value3 > value2 && value3 > value1) {
                //            cout << "value3" << endl;
                max_value2 = value3;
                max_pitch2 = pitch;
                max_yaw2 = yaw;
                
                pitch=pitch-1;
                yaw=yaw-1;
            }
            else if(value2 > max_value2 && value2 > value3 && value2 > value1){
                //            cout << "value2" << endl;
                max_value2 = value2;
                
                max_pitch2 = pitch;
                max_yaw2 = yaw;
                
                pitch=pitch;
                yaw=yaw-1;
                
            }
            else if(value1 > max_value2 && value1 > value3 && value1 > value2){
                //            cout << "value1" << endl;
                max_value2 = value1;
                
                max_pitch2 = pitch;
                max_yaw2 = yaw;
                
                pitch=pitch-1;
                yaw=yaw;
            }
            else{
                search = false; //ループ終了
            }
        }
        
        if (max_value1 >= max_value2) {
            max_value=max_value1;
            max_pitch=max_pitch1;
            max_yaw=max_yaw1;
        }
        else{
            max_value=max_value2;
            max_pitch=max_pitch2;
            max_yaw=max_yaw2;
        }
        
        
#endif
        
#ifdef DEBUG
        //            cout << "Estiamted iris pose: pitch=" << max_pitch <<" "<<"yaw=" << max_yaw << endl;
#endif
        // Draw the estimated result. 推定結果を描画
        tmp.copyTo(img);
        estimated_irispos=rotateEyeModel(max_pitch,max_yaw,iris3_pos);
        ProjectedIrisArea(img,estimated_irispos, iris_pt);
        
        //Estimating iris center on corneal surface. 角膜表面上の虹彩中心推定
        LrefPos = EstimateIrisCenterOnCornea();
        ProjectedIrisArea(img, LrefPos);
        
        if (showModel_flg==false) {
             glwindow = initalize();
            boost::thread thr_eyemodel(&showModel);
            showModel_flg=true;
        }
        glfwPollEvents();
        send_parameters(shm,max_yaw,max_pitch);//write info to shared mem. 共有メモリに書き出し
        
        if (Unwarped_flg==true) {
            cv::Mat tmp;
            org.copyTo(tmp);
            boost::thread thr_unwarping(boost::bind(&UnwarpingImage,tmp,LrefPos));
            Unwarped_flg=false;
        }
        cv::imshow("Camera", img);
        
        if(UnwarpedImg_flg==true){
            cv::imshow("Processing", unwarp);
//#ifdef SCENE
//            //テンプレートマッチング
//            cv::matchTemplate(scene, unwarp, result, CV_TM_CCOEFF_NORMED);
//            normalize( result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );
//            
//            /// Localizing the best match with minMaxLoc
//            double minVal; double maxVal;cv::Point minLoc; cv::Point maxLoc;
//            cv::minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );
//            
//            /// Show me what you got
//            rectangle( scene, minLoc, cv::Point( minLoc.x + unwarp.cols , minLoc.y + unwarp.rows ), cv::Scalar::all(0), 2, 8, 0 );
//            cv::imshow("Scene", scene);
//#endif
//#ifdef DESKTOP
//            //テンプレートマッチング
//            cv::matchTemplate(resizeimage, unwarp, result, CV_TM_CCOEFF_NORMED);
//            normalize( result, result, 0, 1, cv::NORM_MINMAX, -1, cv::Mat() );
//            
//            /// Localizing the best match with minMaxLoc
//            double minVal; double maxVal;cv::Point minLoc; cv::Point maxLoc;
//            cv::minMaxLoc( result, &minVal, &maxVal, &minLoc, &maxLoc, cv::Mat() );
//            
//            /// Show me what you got
//            rectangle(resizeimage, minLoc, cv::Point( minLoc.x + unwarp.cols , minLoc.y + unwarp.rows ), cv::Scalar::all(0), 2, 8, 0 );
//            
//            cv::imshow("Desktop", resizeimage);
//#endif
            UnwarpedImg_flg=false;
        }
        
        cv::waitKey(-1);//30連続
        
    }
#endif
    
    
    
    
    return 0;
}
