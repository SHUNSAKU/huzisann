//
//  eyeball.cpp
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/08/19.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.

//pupilで統一limubsは削除

#include <opencv2/opencv.hpp>
#include <iostream>
#include <iomanip>
#include <stdlib.h>

#include "common.h"
#include "eyeball.h"
#include "geometry.h"
#include "axis.h"
#include "equation.h"

using namespace std;


//Eye Paramters;
double rl;//虹彩半径
double dlc;//虹彩中心から角膜球中心
double rc;
double rcl;
double re;//眼球半径

//Eye Model;
double d;//カメラから瞳孔中心
cv::Mat L;//虹彩中心
cv::Mat g;//注視ベクトル
cv::Mat C;//角膜中心
cv::Mat CC; //眼球中心
cv::Mat L2;//移動後の虹彩中心

//cv::Mat LrefPos;

cv::Mat EyeCorner1;
cv::Mat EyeCorner2;
cv::Mat LCorner;
cv::Mat RCorner;
cv::Mat eyeY;
cv::Mat eyeX;
cv::Mat eyeZ;

std::vector<cv::Mat> iris_vmat;



//Camera Paramters;
extern double MMPERPIXEL;
extern double focus;
extern int cx;
extern int cy;


//入力画像平面
extern cv::Mat NI;
extern cv::Mat P0;
extern double D;


//OpenGL用の色指定
extern double black[];
extern double white[];
extern double red[];
extern double green[];
extern double zero[];
extern double brown[];
extern double blue[];

void set_eye_paramter(){
    rl=5.6;//虹彩の半径
    dlc = 5.6;
    rc=7.8;//角膜球の半径
    rcl=13;//
    re= 25.1-rcl;
}

void CreateEyeModel(cv::RotatedRect &box){

    //この辺りを確認する必要がある．バグにつながるので
    double rmax=box.size.height/2;
    double rmin=box.size.width/2;
    double phi= (box.angle - 90) /180 * M_PI;
    
    //Calc cornea and eye parameters
    d=focus * rl/(MMPERPIXEL*rmax);
    L = (cv::Mat_<double>(3,1,CV_64F) << d * MMPERPIXEL*(box.center.x-cx)/focus,d * MMPERPIXEL*(box.center.y-cy)/focus,d);//3D iris center;
    double tau = acos(rmin/rmax);
    g= (cv::Mat_<double>(3,1,CV_64F) <<sin(tau)*sin(phi),-sin(tau)*cos(phi),-cos(tau));//gaze vector
    C= -dlc*g/cv::norm(g)+L; //角膜球
    CC= -(rcl-rc)*g/norm(g)+C;
    
#ifdef DEBUG
    cout<<"r_max="<<rmax<<endl;
    cout<<"r_min="<<rmin<<endl;
    cout<<"phi="<<phi<<endl;
    cout << "d="<<d<<endl;
    cout << "focus="<<focus<<endl;
    cout << "L="<<L<<endl;
    cout << "tau="<<tau<<endl;
    cout << "g="<<g<<endl;
    cout << "C="<<C<<endl;
    cout << "CC="<<CC<<endl;
    
#endif
    
}

void CalcEyeAxis(cv::Point2f &left, cv::Point2f &right){
    //Calc axis
    EyeCorner1  = (cv::Mat_<double>(3,1) << d * MMPERPIXEL * (left.x -cx)/focus, d * MMPERPIXEL * (left.y-cy)/focus,d);
    EyeCorner2  = (cv::Mat_<double>(3,1) << d * MMPERPIXEL * (right.x -cx)/focus, d * MMPERPIXEL * (right.y-cy)/focus,d);
    LCorner = EyeCorner1-CC;
    RCorner = EyeCorner2-CC;

    //眼球中心座標系で計算(オリジナルと変更しているので，どちらが適切か要確認)
    cv::Mat L_CC = L-CC;
    eyeY =LCorner.cross(L_CC)/cv::norm(LCorner.cross(L_CC));
    eyeX= (L_CC).cross(eyeY)/cv::norm((L_CC).cross(eyeY));
    eyeZ =L_CC/norm(L_CC);

    //カメラ座標系に戻す
    eyeY =20*eyeY+CC;
    eyeX= 20*eyeX+CC;
    eyeZ =20*eyeZ+CC;
}

std::vector<cv::Mat> CalcLimbus(){
    iris_vmat=CalcIntersectionSphere2Sphere(C,rc, CC,re);

/*
    vector<cv::Mat>::iterator it_s;
    for (it_s= iris_vmat.begin();it_s<iris_vmat.end(); it_s++){
        cout <<"x="<<*it_s->ptr<double>(0) <<"y="<< *it_s->ptr<double>(1)<<"z="<<*it_s->ptr<double>(2)<<endl;
    }
 */
    return iris_vmat;
}

void showEyeModel(cv::Mat &LrefPos, double pitch, double yaw){
    //    double cornea_pos[]={0,0,d+focus};
    
    //眼球中心を回旋中心として回転
    cv::Mat rvec0(3,1,CV_64FC1);
    cv::Mat rvec1(3,1,CV_64FC1);
    
    rvec0= M_PI*(pitch/180)* (eyeX-CC)/cv::norm(eyeX-CC);
    rvec1= M_PI*(yaw/180)* (eyeY-CC)/cv::norm(eyeY-CC);
    
    cv::Mat RotX(3,3,CV_64FC1);
    cv::Mat RotY(3,3,CV_64FC1);
    cv::Rodrigues(rvec0,RotX);
    cv::Rodrigues(rvec1,RotY);
    
    cv::Mat C2=RotX*RotY*(C-CC)+CC;
    cv::Mat L2=RotX*RotY*(L-CC)+CC;
    cv::Mat eyeX2=RotX*RotY*(eyeX-CC)+CC;
    cv::Mat eyeY2=RotX*RotY*(eyeY-CC)+CC;
    cv::Mat eyeZ2=RotX*RotY*(eyeZ-CC)+CC;
    
    double cornea_pos[]={C2.at<double>(0),C2.at<double>(1),C2.at<double>(2)};
    drawSphere(cornea_pos,rc,brown);
    double eye_pos[]={CC.at<double>(0),CC.at<double>(1),CC.at<double>(2)};
//    drawSphere(eye_pos,re,white);
    double iris_center_pos[]={L2.at<double>(0),L2.at<double>(1),L2.at<double>(2)};
    drawSphere(iris_center_pos,1.0,red);

    //座標軸決定のために登録した
    //showVec(CC.ptr<double>(0),EyeCorner1.ptr<double>(0), black);
    //showVec(CC.ptr<double>(0),EyeCorner2.ptr<double>(0), black);
    
    //眼球の回転軸の表示
    showVec(CC.ptr<double>(0),eyeX2.ptr<double>(0),red);
    showVec(CC.ptr<double>(0),eyeY2.ptr<double>(0),green);
    showVec(CC.ptr<double>(0),eyeZ2.ptr<double>(0),blue);
    
    //Limbusの表示
    vector<cv::Mat>::iterator it_s;
    for (it_s= iris_vmat.begin();it_s<iris_vmat.end(); it_s++){
//        double limbus_pos[]={*it_s->ptr<double>(0),*it_s->ptr<double>(1),*it_s->ptr<double>(2)};
        cv::Mat limbus=(cv::Mat_<double>(3,1)<< *it_s->ptr<double>(0),*it_s->ptr<double>(1),*it_s->ptr<double>(2));
        cv::Mat limbus2=RotX*RotY*(limbus-CC)+CC;
        double limbus_pos[]={limbus2.at<double>(0),limbus2.at<double>(1),limbus2.at<double>(2)};

        drawSphere(limbus_pos, 0.5, blue);
    }
    
    double lc_pos[]={LrefPos.at<double>(0),LrefPos.at<double>(1),LrefPos.at<double>(2)};
    drawSphere(lc_pos,1.0,red);
}


std::vector<cv::Mat> rotateEyeModel(double pitch, double yaw, std::vector<cv::Mat> iris3){

    //眼球中心を回旋中心として回転
    cv::Mat rvec0(3,1,CV_64FC1);
    cv::Mat rvec1(3,1,CV_64FC1);

    rvec0= M_PI*(pitch/180)* (eyeX-CC)/cv::norm(eyeX-CC);
    rvec1= M_PI*(yaw/180)* (eyeY-CC)/cv::norm(eyeY-CC);
    
    cv::Mat RotX(3,3,CV_64FC1);
    cv::Mat RotY(3,3,CV_64FC1);
    cv::Rodrigues(rvec0,RotX);
    cv::Rodrigues(rvec1,RotY);
    
#ifdef DEBUG
//    cout<<"RotX="<<RotX<<endl;
//    cout<<"RotY="<<RotY<<endl;
#endif
    
    std::vector<cv::Mat> rotated_iris3;
    vector<cv::Mat>::iterator it_s;
    for (it_s= iris3.begin();it_s<iris3.end(); it_s++){
        cv::Mat pos = (cv::Mat_<double>(3,1) << *it_s->ptr<double>(0),*it_s->ptr<double>(1),*it_s->ptr<double>(2));
        rotated_iris3.push_back(RotX*RotY*(pos-CC)+CC);
    }

    //虹彩中心を回転
    L2=RotX*RotY*(L-CC)+CC;
    
    return rotated_iris3;
}


cv::Mat EstimateIrisCenterOnCornea(){
#ifdef DEBUG
//    cout <<"C="<<C<<endl;
//    cout <<"L2="<<L2<< endl;
#endif
    
    cv::Mat LrefPos=IntersectionRay2Sphere(L2, C, rc);
    return LrefPos;
}


//平面の決め方がこれで良いのか(MATLAB含め要確認)
//平面を定義してから，回転並進移動の方が適切では？
cv::Mat SetPlane(cv::Mat &LrefPos){
    cv::Mat plane(HEIGHT+1,WIDTH+1,CV_64FC3);
    cv::Mat normalPor = (LrefPos-C)/cv::norm(LrefPos-C);
    double x,y;
    int m,n;
    for (n=0,y=SCALE*MMPERPIXEL*(-HEIGHT/2)+LrefPos.at<double>(1); y <SCALE*MMPERPIXEL*(HEIGHT/2)+LrefPos.at<double>(1); y=y+(SCALE*MMPERPIXEL),n++) {
        for (m=0,x=SCALE*MMPERPIXEL*(-WIDTH/2)+LrefPos.at<double>(0); x<SCALE*MMPERPIXEL*(WIDTH/2)+LrefPos.at<double>(0); x=x+(SCALE*MMPERPIXEL),m++) {
            double d = -(normalPor.at<double>(0)*LrefPos.at<double>(0)+normalPor.at<double>(1)*LrefPos.at<double>(1)+normalPor.at<double>(2)*LrefPos.at<double>(2));
            double z = -(normalPor.at<double>(0)*x+normalPor.at<double>(1)*y+d)/normalPor.at<double>(2);
//            cout << "n="<<n<<" "<<"m="<<m<<" "<<"x="<<x<<" "<<"y="<<y<<endl;
            plane.at<cv::Vec3d>(n,m)[0]=x;
            plane.at<cv::Vec3d>(n,m)[1]=y;
            plane.at<cv::Vec3d>(n,m)[2]=z;
        }
    }
    return plane;
}



cv::Mat CalcurateReflectionVec(cv::Mat &plane){

    cv::Mat Point(HEIGHT+1,WIDTH+1,CV_64FC3);
    cv::Mat O= cv::Mat::zeros(3, 1, CV_64FC1);
    cv::Mat RefPoint;

    cv::Mat S=O-C;
    cv::Mat PointTmp;
    double a=S.dot(S);

    for (int y=0; y<HEIGHT+1; y++) {
        for (int x=0; x<WIDTH+1; x++) {
//            cout << "x=" << x <<" " << "y=" << y << endl;
            cv::Mat Q=(cv::Mat_<double>(3,1)<< plane.at<cv::Vec3d>(y,x)[0],plane.at<cv::Vec3d>(y,x)[1],plane.at<cv::Vec3d>(y,x)[2]);

//            cv::Mat RefVec = (Q-C)/cv::norm(Q-C);
            cv::Mat RefVec = (Q-C);
//            cout << "RefVec="<<RefVec<<endl;
            double b= S.dot(RefVec);
            double c= RefVec.dot(RefVec);
            double d= cv::norm(S.cross(RefVec))*cv::norm(S.cross(RefVec));


//            cout <<"RefVec="<<RefVec<<" c="<<c<<endl;
//            cout <<"a="<<a<<" "<<"b="<<b<<" "<<"c="<<c<<" "<<"d="<<d<<endl;
            
            double e[5];
            double ans_x=0;
            double ansx=0;
            double ans_y=0;
            double ansy=0;

        
            int order=4;


            e[0]=(a-1);
            e[1]=2*(a-b);
            e[2]=(a+2*b+c-4*a*c);
            e[3]=(-4*d);
            e[4]=(4*c*d);

            double r[order*2];
//            roots(e,order+1,order,r,order*2);
            Cans res;
            res = equation4(e[4],e[3], e[2], e[1], e[0]);
            
            for(unsigned int i=0; i<order; i++) {
#if 0
                ans_y = r[i*2];
                ans_x = (-2*c*r[i*2]*r[i*2]+r[i*2]+1)/(2*b*r[i*2]+1);
#else
                ans_y = res.ans[i].r;
                ans_x = (-2*c*res.ans[i].r*res.ans[i].r+res.ans[i].r+1)/(2*b*res.ans[i].r+1);
#endif
                if(ans_x >=0 && ans_y >= 0){
                    ansx=ans_x;
                    ansy=ans_y;
                }
            }

            

            if (ansx!=0 && ansy!=0) {
                cv::Mat RefNormal=ansx*S+ansy*RefVec;
                RefPoint=RefNormal/cv::norm(RefNormal)*rc+C;
                double t= -(D)/NI.dot((RefPoint)/cv::norm(RefPoint));
                PointTmp= t * (RefPoint)/cv::norm(RefPoint);

                Point.at<cv::Vec3d>(y,x)[0]=PointTmp.at<double>(0);
                Point.at<cv::Vec3d>(y,x)[1]=PointTmp.at<double>(1);
                Point.at<cv::Vec3d>(y,x)[2]=PointTmp.at<double>(2);
            }

            else{
               /*バグ回避のため無理やり補間している*/
                if (PointTmp.cols !=0) {
                    Point.at<cv::Vec3d>(y,x)[0]=PointTmp.at<double>(0);
                    Point.at<cv::Vec3d>(y,x)[1]=PointTmp.at<double>(1);
                    Point.at<cv::Vec3d>(y,x)[2]=PointTmp.at<double>(2);
                }
                else{
                    Point.at<cv::Vec3d>(y,x)[0]=0;
                    Point.at<cv::Vec3d>(y,x)[1]=0;
                    Point.at<cv::Vec3d>(y,x)[2]=0;
                }
            }

        }
    }
    return Point;

}

