//
//  sharedmem.cpp
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/11/29.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include "sharedmem.h"


char*
InitSharedMem(int key, int size)
{
    int shmid;
    char *p;
    
    if ((shmid = shmget(key, 1, IPC_CREAT | 0666)) == EOF){
        perror("ShmGet Error");
        //    fprintf(stderr, "ShmGet Error\n");
        //    exit(0);
    }
    if ((p = (char*)shmat(shmid, NULL, 0)) == (void*)-1) {
        fprintf(stderr, "ShmAt Error\n");
        exit(-1);
    }
    memset(p, 0, size);
    return p;
}


void
send_parameters(char *shm, float yaw_angle, float pitch_angle)
{
    SendData *data;
    
    data = (SendData *)shm;
    
    (*data).yaw_angle = yaw_angle;
    (*data).pitch_angle = pitch_angle;
}
