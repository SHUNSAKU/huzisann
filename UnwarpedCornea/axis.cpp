//
//  axis.cpp
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/08/19.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//
//
// OpenGL関連の描画をまとめたものなので，OpenGL以外のライブラリに依存しないように記述したい．
//

#include <GLFW/glfw3.h>
#include <OpenGL/glu.h>
#include <opencv2/opencv.hpp>
#include "axis.h"

GLdouble vertex[][3] = {
	{ 0.0, 0.0, 0.0 },
	{ 10.0, 0.0, 0.0 },
	{ 0.0, 0.0, 0.0 },
	{ 0.0, 10.0, 0.0 },
	{ 0.0, 0.0, 0.0 },
	{ 0.0, 0.0, 10.0 }
};

int edge[][2] = {
	{ 0, 1 },
	{ 2, 3 },
	{ 4, 5 },
};

static const GLfloat aLightColor[] = { 0.2, 0.2, 0.8, 1.0 };// 光源の色。

extern double d;
extern double focus;

GLFWwindow* initalize(){
    //Initialize for OpenGL
    if(! glfwInit() ){
        std::cerr << "glfwInit failed." << std::endl;
        exit( EXIT_FAILURE );
    }
    
    GLFWwindow* glwindow= glfwCreateWindow(640, 480, "Eye Model", nullptr, nullptr);
    if(! glwindow )
    {
        std::cerr << "glfwCreateWindow failed." << std::endl;
        glfwTerminate();
        exit( EXIT_FAILURE );
    }
    glfwMakeContextCurrent(glwindow);
    glEnable( GL_DEPTH_TEST );// デプスバッファの有効化。
    glLightfv( GL_LIGHT1, GL_DIFFUSE, aLightColor );// 光源1の色を設定。
    glLightfv( GL_LIGHT1, GL_SPECULAR, aLightColor );// 光源1の色を設定。
    glClearColor( 0.5, 0.5, 0.5, 1 );// glClear() で使用する色（RGBA）
    
    return glwindow;
}


void drawGridPlaneXY()
{
    glPushMatrix();
    float   m_xMin=-10, m_xMax=10;
    float   m_yMin=-10, m_yMax=10;
    float   m_plane_z=0;
    float   m_frequency=0.1;
    glLineWidth(1);
    glBegin(GL_LINES);
    glColor3f(1.0,1.0,1.0);
    
    for (float y=m_yMin;y<=m_yMax;y+=m_frequency){
        glVertex3f( m_xMin,y,m_plane_z );
        glVertex3f( m_xMax,y,m_plane_z );
    }
    
    for (float x=m_xMin;x<=m_xMax;x+=m_frequency){
        glVertex3f( x,m_yMin,m_plane_z );
        glVertex3f( x,m_yMax,m_plane_z );
    }
    
    glEnd();
    glPopMatrix();
}


void drawGridPlaneXZ(){
    glPushMatrix();

    float   m_xMin=-1000, m_xMax=1000;
    float   m_zMin=-1000, m_zMax=1000;
    float   m_plane_y=100;//少しカメラ位置よりしたに描画
    float   m_frequency=10;
    
    glLineWidth(1);
    glBegin(GL_LINES);
    glColor3f(1.0,1.0,1.0);
    for (float z=m_zMin;z<=m_zMax;z+=m_frequency)
    {
        glVertex3f( m_xMin,m_plane_y,z );
        glVertex3f( m_xMax,m_plane_y,z );
    }
    
    for (float x=m_xMin;x<=m_xMax;x+=m_frequency)
    {
        glVertex3f( x,m_plane_y,m_zMin );
        glVertex3f( x,m_plane_y,m_zMax );
    }
    
    
    glEnd();
    glPopMatrix();
}

void drawAxisXYZ(double *pos){
    glPushMatrix();
    glTranslated(pos[0], pos[1], pos[2]);
    GLfloat color[][3]={
        { 1.0, 0.0, 0.0 },
        { 0.0, 1.0, 0.0 },
        { 0.0, 0.0, 1.0 },
    };
    
    //座標系の描画
    glBegin(GL_LINES);
    for (int i = 0; i < 3; ++i) {
        glColor3f(color[0][i],color[1][i],color[2][i]);
        glVertex3dv(vertex[edge[i][0]]);
        glVertex3dv(vertex[edge[i][1]]);
    }
    glEnd();
    glPopMatrix();
}

void showVec(double *start, double *goal,double *color){
    glPushMatrix();

    //ベクトルの描画
    glBegin(GL_LINES);
        glColor3f ( color[0], color[1], color[2] );
        glVertex3dv(start);
        glVertex3dv(goal);
    glEnd();
    
    glPopMatrix();
}





void drawSphere(double *pos, double radius, double *color){
    glPushMatrix();
    glColor3f ( color[0], color[1], color[2] );
    glTranslated(pos[0],pos[1],pos[2]);
    GLUquadricObj *sphere = gluNewQuadric(); //オブジェクト生成
//    gluQuadricDrawStyle(sphere, GLU_LINE); //ワイヤーフレームを指定
    gluQuadricDrawStyle(sphere, GLU_FILL);
//    gluQuadricDrawStyle(sphere, GLU_SILHOUETTE);
    gluSphere(sphere, radius, 50.0, 50.0);
    glPopMatrix();
}



void drawImagePlane(double *color, double width, double height, double *pos  ){
    glColor3f ( color[0],color[1], color[2] );
    glPushMatrix();
    
    glTranslated(pos[0], pos[1], pos[2]);
    
    glBegin(GL_LINE_LOOP);
    glVertex3f(-1*width/2, -1*height/2, 0);
    glVertex3f(-1*width/2,  1*height/2, 0);
    glVertex3f( 1*width/2,  1*height/2, 0);
    glVertex3f( 1*width/2, -1*height/2, 0);
    glVertex3f(-1*width/2, -1*height/2, 0);
    

    glEnd();
    glPopMatrix();
}

void drawTangentPlane(cv::Mat &plane){
    for (int y=0; y<plane.rows; y=y+10) {
        for (int x=0; x<plane.cols; x=x+10) {
            double pos[3]={plane.at<cv::Vec3d>(y,x)[0],plane.at<cv::Vec3d>(y,x)[1],plane.at<cv::Vec3d>(y,x)[2]};
            double color[3]={0,1.0,0};
            drawSphere(pos, 0.5, color);

        }
    }
}

