//
//  equation.cpp
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/10/07.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#include "equation.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_eigen.h>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


static const Cdouble _C0 = { 0.0, 0.0 };



int roots(double *a, int len_a, int order, double *r, int len_r) {
    
    /*----- 引数チェック -----*/
    if (len_a < order+1) {
//        fprintf(stderr, "must be a >= order+1\n");
        return -1;
    }
    
    if (len_r < order*2) {
//        fprintf(stderr, "must be a >= order*2\n");
        return -2;
    }
    
    /*----- 準備 -----*/
    gsl_matrix *M = gsl_matrix_alloc(order, order);
    gsl_vector_complex *eval = gsl_vector_complex_alloc(order);
    gsl_matrix_complex *evec = gsl_matrix_complex_alloc(order, order);
    gsl_eigen_nonsymmv_workspace *w = gsl_eigen_nonsymmv_alloc(order);
    int i, j;
    
    
    /*----- コンパニオン行列をつくる -----*/
    // ゼロで埋める
    for (i=0; i<order; i++) {
        for (j=0; j<order; j++) {
            gsl_matrix_set(M, i, j, 0.0);
        }
    }
    
    // 単位行列部分
    for (i=0; i<order-1; i++) {
        for (j=0; j<order-1; j++) {
            if (i == j) {
                gsl_matrix_set(M, i, j+1, 1.0);
            }
        }
    }
    
    // 係数部分
    for (j=0; j<order; j++) {
        gsl_matrix_set(M, order-1, j, -1.0*a[j]/a[order]);
    }
    
    
    /*----- 固有値を求める -----*/
    // 固有値,固有ベクトルの計算
    gsl_eigen_nonsymmv(M, eval, evec, w);
    
    // 固有値の絶対値でソート
    gsl_eigen_nonsymmv_sort(eval, evec, GSL_EIGEN_SORT_ABS_DESC);
    
    for (i=0; i<order; i++) {
        r[2*i] = eval->data[2*i];
        r[2*i+1] = eval->data[2*i+1];
    }
    
    
    /*----- 後始末 -----*/
    gsl_eigen_nonsymmv_free(w);
    gsl_matrix_free(M);
    gsl_vector_complex_free(eval);
    gsl_matrix_complex_free(evec);
    
    return 0;
}
// poly.cpp : solution of cubic and quartic equation
// (c) Khashin S.I. http://math.ivanovo.ac.ru/dalgebra/Khashin/index.html
// khash2 (at) gmail.com
//
#include <math.h>

//#include "vUtil.h"      // ‡ÁÌ˚Â ÏÂÎÍËÂ ÙÛÌÍˆËË
//#include "poly34.h"     // solution of cubic and quartic equation
#define	TwoPi  6.28318530717958648
const double eps=1e-14;
//---------------------------------------------------------------------------
// x - array of size 3
// In case 3 real roots: => x[0], x[1], x[2], return 3
//         2 real roots: x[0], x[1],          return 2
//         1 real root : x[0], x[1] ± i*x[2], return 1
int SolveP3(double *x,double a,double b,double c) {	// solve cubic equation x^3 + a*x^2 + b*x + c
    double a2 = a*a;
    double q  = (a2 - 3*b)/9;
    double r  = (a*(2*a2-9*b) + 27*c)/54;
    double r2 = r*r;
    double q3 = q*q*q;
    double A,B;
    if(r2<q3) {
        double t=r/sqrt(q3);
        if( t<-1) t=-1;
        if( t> 1) t= 1;
        t=acos(t);
        a/=3; q=-2*sqrt(q);
        x[0]=q*cos(t/3)-a;
        x[1]=q*cos((t+TwoPi)/3)-a;
        x[2]=q*cos((t-TwoPi)/3)-a;
        return(3);
    } else {
        A =-pow(fabs(r)+sqrt(r2-q3),1./3);
        if( r<0 ) A=-A;
        B = A==0? 0 : B=q/A;
        
        a/=3;
        x[0] =(A+B)-a;
        x[1] =-0.5*(A+B)-a;
        x[2] = 0.5*sqrt(3.)*(A-B);
        if(abs(x[2])<eps) { x[2]=x[1]; return(2); }
        return(1);
    }
}// SolveP3(double *x,double a,double b,double c) {
//---------------------------------------------------------------------------
// a>=0!
void  CSqrt( double x, double y, double &a, double &b) // returns:  a+i*s = sqrt(x+i*y)
{
    double r  = sqrt(x*x+y*y);
    if( y==0 ) {
        r = sqrt(r);
        if(x>=0) { a=r; b=0; } else { a=0; b=r; }
    } else {		// y != 0
        a = sqrt(0.5*(x+r));
        b = 0.5*y/a;
    }
}
//---------------------------------------------------------------------------
int   SolveP4Bi(double *x, double b, double d)	// solve equation x^4 + b*x^2 + d = 0
{
    double D = b*b-4*d;
    if( D>=0 )
    {
        double sD = sqrt(D);
        double x1 = (-b+sD)/2;
        double x2 = (-b-sD)/2;	// x2 <= x1
        if( x2>=0 )				// 0 <= x2 <= x1, 4 real roots
        {
            double sx1 = sqrt(x1);
            double sx2 = sqrt(x2);
            x[0] = -sx1;
            x[1] =  sx1;
            x[2] = -sx2;
            x[3] =  sx2;
            return 4;
        }
        if( x1 < 0 )				// x2 <= x1 < 0, two pair of imaginary roots
        {
            double sx1 = sqrt(-x1);
            double sx2 = sqrt(-x2);
            x[0] =    0;
            x[1] =  sx1;
            x[2] =    0;
            x[3] =  sx2;
            return 0;
        }
        // now x2 < 0 <= x1 , two real roots and one pair of imginary root
        double sx1 = sqrt( x1);
        double sx2 = sqrt(-x2);
        x[0] = -sx1;
        x[1] =  sx1;
        x[2] =    0;
        x[3] =  sx2;
        return 2;
    } else { // if( D < 0 ), two pair of compex roots
        double sD2 = 0.5*sqrt(-D);
        CSqrt(-0.5*b, sD2, x[0],x[1]);
        CSqrt(-0.5*b,-sD2, x[2],x[3]);
        return 0;
    } // if( D>=0 )
} // SolveP4Bi(double *x, double b, double d)	// solve equation x^4 + b*x^2 d
//---------------------------------------------------------------------------
#define SWAP(a,b) { t=b; b=a; a=t; }
static void  dblSort3( double &a, double &b, double &c) // make: a <= b <= c
{
    double t;
    if( a>b ) SWAP(a,b);	// now a<=b
    if( c<b ) {
        SWAP(b,c);			// now a<=b, b<=c
        if( a>b ) SWAP(a,b);// now a<=b
    }
}
//---------------------------------------------------------------------------
int   SolveP4De(double *x, double b, double c, double d)	// solve equation x^4 + b*x^2 + c*x + d
{
    //if( c==0 ) return SolveP4Bi(x,b,d); // After that, c!=0
    if( fabs(c)<1e-14*(fabs(b)+fabs(d)) ) return SolveP4Bi(x,b,d); // After that, c!=0
    
    int res3 = SolveP3( x, 2*b, b*b-4*d, -c*c);	// solve resolvent
    // by Viet theorem:  x1*x2*x3=-c*c not equals to 0, so x1!=0, x2!=0, x3!=0
    if( res3>1 )	// 3 real roots,
    {
        dblSort3(x[0], x[1], x[2]);	// sort roots to x[0] <= x[1] <= x[2]
        // Note: x[0]*x[1]*x[2]= c*c > 0
        if( x[0] > 0) // all roots are positive
        {
            double sz1 = sqrt(x[0]);
            double sz2 = sqrt(x[1]);
            double sz3 = sqrt(x[2]);
            // Note: sz1*sz2*sz3= -c (and not equal to 0)
            if( c>0 )
            {
                x[0] = (-sz1 -sz2 -sz3)/2;
                x[1] = (-sz1 +sz2 +sz3)/2;
                x[2] = (+sz1 -sz2 +sz3)/2;
                x[3] = (+sz1 +sz2 -sz3)/2;
                return 4;
            }
            // now: c<0
            x[0] = (-sz1 -sz2 +sz3)/2;
            x[1] = (-sz1 +sz2 -sz3)/2;
            x[2] = (+sz1 -sz2 -sz3)/2;
            x[3] = (+sz1 +sz2 +sz3)/2;
            return 4;
        } // if( x[0] > 0) // all roots are positive
        // now x[0] <= x[1] < 0, x[2] > 0
        // two pair of comlex roots
        double sz1 = sqrt(-x[0]);
        double sz2 = sqrt(-x[1]);
        double sz3 = sqrt( x[2]);
        
        if( c>0 )	// sign = -1
        {
            x[0] = -sz3/2;
            x[1] = ( sz1 -sz2)/2;		// x[0]±i*x[1]
            x[2] =  sz3/2;
            x[3] = (-sz1 -sz2)/2;		// x[2]±i*x[3]
            return 0;
        }
        // now: c<0 , sign = +1
        x[0] =   sz3/2;
        x[1] = (-sz1 +sz2)/2;
        x[2] =  -sz3/2;
        x[3] = ( sz1 +sz2)/2;
        return 0;
    } // if( res3>1 )	// 3 real roots,
    // now resoventa have 1 real and pair of compex roots
    // x[0] - real root, and x[0]>0,
    // x[1]±i*x[2] - complex roots,
    double sz1 = sqrt(x[0]);
    double szr, szi;
    CSqrt(x[1], x[2], szr, szi);  // (szr+i*szi)^2 = x[1]+i*x[2]
    if( c>0 )	// sign = -1
    {
        x[0] = -sz1/2-szr;			// 1st real root
        x[1] = -sz1/2+szr;			// 2nd real root
        x[2] = sz1/2;
        x[3] = szi;
        return 2;
    }
    // now: c<0 , sign = +1
    x[0] = sz1/2-szr;			// 1st real root
    x[1] = sz1/2+szr;			// 2nd real root
    x[2] = -sz1/2;
    x[3] = szi;
    return 2;
} // SolveP4De(double *x, double b, double c, double d)	// solve equation x^4 + b*x^2 + c*x + d
//-----------------------------------------------------------------------------
double N4Step(double x, double a,double b,double c,double d)	// one Newton step for x^4 + a*x^3 + b*x^2 + c*x + d
{
    double fxs= ((4*x+3*a)*x+2*b)*x+c;	// f'(x)
    if( fxs==0 ) return 1e99;
    double fx = (((x+a)*x+b)*x+c)*x+d;	// f(x)
    return x - fx/fxs;
}
//-----------------------------------------------------------------------------
// x - array of size 4
// return 4: 4 real roots x[0], x[1], x[2], x[3], possible multiple roots
// return 2: 2 real roots x[0], x[1] and complex x[2]±i*x[3],
// return 0: two pair of complex roots: x[0]±i*x[1],  x[2]±i*x[3],
int   SolveP4(double *x,double a,double b,double c,double d) {	// solve equation x^4 + a*x^3 + b*x^2 + c*x + d by Dekart-Euler method
    // move to a=0:
    d += 0.25*a*( 0.25*b*a - 3./64*a*a*a - c);
    c += 0.5*a*(0.25*a*a - b);
    b -= 0.375*a*a;
    int res = SolveP4De( x, b, c, d);
    if( res==4) { x[0]-= a/4; x[1]-= a/4; x[2]-= a/4; x[3]-= a/4; }
    else if (res==2) { x[0]-= a/4; x[1]-= a/4; x[2]-= a/4; }
    else             { x[0]-= a/4; x[2]-= a/4; }
    // one Newton step for each real root:
    if( res>0 )
    {
        x[0] = N4Step(x[0], a,b,c,d);
        x[1] = N4Step(x[1], a,b,c,d);
    }
    if( res>2 )
    {
        x[2] = N4Step(x[2], a,b,c,d);
        x[3] = N4Step(x[3], a,b,c,d);
    }
    return res;
}
//-----------------------------------------------------------------------------
#define F5(t) (((((t+a)*t+b)*t+c)*t+d)*t+e)
//-----------------------------------------------------------------------------
double SolveP5_1(double a,double b,double c,double d,double e)	// return real root of x^5 + a*x^4 + b*x^3 + c*x^2 + d*x + e = 0
{
    int cnt;
    if( abs(e)<eps ) return 0;
    
    double brd =  abs(a);			// brd - border of real roots
    if( abs(b)>brd ) brd = abs(b);
    if( abs(c)>brd ) brd = abs(c);
    if( abs(d)>brd ) brd = abs(d);
    if( abs(e)>brd ) brd = abs(e);
    brd++;							// brd - border of real roots
    
    double x0, f0;					// less, than root
    double x1, f1;					// greater, than root
    double x2, f2, f2s;				// next values, f(x2), f'(x2)
    double dx;
    
    if( e<0 ) { x0 =   0; x1 = brd; f0=e; f1=F5(x1); x2 = 0.01*brd; }
    else	  { x0 =-brd; x1 =   0; f0=F5(x0); f1=e; x2 =-0.01*brd; }
    
    if( abs(f0)<eps ) return x0;
    if( abs(f1)<eps ) return x1;
    
    // now x0<x1, f(x0)<0, f(x1)>0
    // Firstly 5 bisections
    for( cnt=0; cnt<5; cnt++)
    {
        x2 = (x0 + x1)/2;			// next point
        f2 = F5(x2);				// f(x2)
        if( abs(f2)<eps ) return x2;
        if( f2>0 ) { x1=x2; f1=f2; }
        else       { x0=x2; f0=f2; }
    }
    
    // At each step:
    // x0<x1, f(x0)<0, f(x1)>0.
    // x2 - next value
    // we hope that x0 < x2 < x1, but not necessarily
    do {
        cnt++;
        if( x2<=x0 || x2>= x1 ) x2 = (x0 + x1)/2;	// now  x0 < x2 < x1
        f2 = F5(x2);								// f(x2)
        if( abs(f2)<eps ) return x2;
        if( f2>0 ) { x1=x2; f1=f2; }
        else       { x0=x2; f0=f2; }
        f2s= (((5*x2+4*a)*x2+3*b)*x2+2*c)*x2+d;		// f'(x2)
        if( abs(f2s)<eps ) { x2=1e99; continue; }
        dx = f2/f2s;
        x2 -= dx;
    } while(abs(dx)>eps);
    return x2;
} // SolveP5_1(double a,double b,double c,double d,double e)	// return real root of x^5 + a*x^4 + b*x^3 + c*x^2 + d*x + e = 0
//-----------------------------------------------------------------------------
int   SolveP5(double *x,double a,double b,double c,double d,double e)	// solve equation x^5 + a*x^4 + b*x^3 + c*x^2 + d*x + e = 0
{
    double r = x[0] = SolveP5_1(a,b,c,d,e);
    double a1 = a+r, b1=b+r*a1, c1=c+r*b1, d1=d+r*c1;
    return 1+SolveP4(x+1, a1,b1,c1,d1);
} // SolveP5(double *x,double a,double b,double c,double d,double e)	// solve equation x^5 + a*x^4 + b*x^3 + c*x^2 + d*x + e = 0
//-----------------------------------------------------------------------------
// let f(x ) = a*x^2 + b*x + c and 
//     f(x0) = f0,
//     f(x1) = f1,
//     f(x2) = f3
// Then r1, r2 - root of f(x)=0.
// Returns 0, if there are no roots, else return 2.
int Solve2( double x0, double x1, double x2, double f0, double f1, double f2, double &r1, double &r2)
{
    double w0 = f0*(x1-x2);
    double w1 = f1*(x2-x0);
    double w2 = f2*(x0-x1);
    double a1 =  w0         + w1         + w2;
    double b1 = -w0*(x1+x2) - w1*(x2+x0) - w2*(x0+x1);
    double c1 =  w0*x1*x2   + w1*x2*x0   + w2*x0*x1;
    double Di =  b1*b1 - 4*a1*c1;	// must be>0!
    if( Di<0 ) { r1=r2=1e99; return 0; }
    Di =  sqrt(Di);
    r1 =  (-b1 + Di)/2/a1;
    r2 =  (-b1 - Di)/2/a1;
    return 2;
}
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------



static inline Cdouble
_Cdouble(double r, double i)
{
    Cdouble res;
    
    res.r = r;
    res.i = i;
    return res;
}

static inline Cdouble
_Cdouble1(double r)
{
    return _Cdouble(r, 0.0);
}

static inline int
cis0(Cdouble x)
{
    return x.r == 0.0 && x.i == 0.0;
}

static inline Cdouble
cneg(Cdouble x)
{
    x.r = -x.r;
    x.i = -x.i;
    return x;
}

static inline Cdouble
cadd(Cdouble a, Cdouble b)
{
    a.r += b.r;
    a.i += b.i;
    return a;
}

static inline Cdouble
csub(Cdouble a, Cdouble b)
{
    a.r -= b.r;
    a.i -= b.i;
    return a;
}

static inline Cdouble
cmul(Cdouble a, Cdouble b)
{
    double ar = a.r;
    double ai = a.i;
    double br = b.r;
    double bi = b.i;
    
    a.r = ar * br - ai * bi;
    a.i = ar * bi + ai * br;
    return a;
}

static inline Cdouble
cdiv(Cdouble a, Cdouble b)
{
    double ar = a.r;
    double ai = a.i;
    double br = b.r;
    double bi = b.i;
    double bb = br * br + bi * bi;
    
    if (bb == 0.0) {
        fprintf(stderr, "cdiv((%e %e), (%e, %e)) Error: divided by 0.\n",
                ar, ai, br, bi);
        exit(1);
    }
    a.r = (ar * br + ai * bi) / bb;
    a.i = (ai * br - ar * bi) / bb;
    return a;
}

static inline double
Cabs(Cdouble a)
{
    double r = a.r;
    double i = a.i;
    return sqrt(r * r + i * i);
}

Cdouble
Csqrt(Cdouble x)
{
    double r = x.r;
    double i = x.i;
    double len;
    
    if (i == 0.0) {
        if (r >= 0.0)
            return _Cdouble1(sqrt(r));
        else
            return _Cdouble(0.0, sqrt(-r));
    }
    len = sqrt(r * r + i * i);
    x.r = sqrt((len + r) * 0.5);
    i = sqrt((len - r) * 0.5);
    if (i < 0.0) i = -i;
    x.i = i;
    return x;
}

Cdouble
ccbrt(Cdouble x)
{
    double r = x.r;
    double i = x.i;
    double clen, ctheta;
    
    if (i == 0.0)
        return _Cdouble1(cbrt(r));
    
    clen = cbrt(sqrt(r * r + i * i));
    ctheta = atan2(i, r) / 3.0;
    x.r = clen * cos(ctheta);
    x.i = clen * sin(ctheta);
    return x;
}



Cans
equation1(double a, double b)
{
    Cans ans;
    
    ans.ans[0] = ans.ans[1] = ans.ans[2] = ans.ans[3] = _C0;
    if (a == 0.0) {
        if (b == 0.0)
            ans.n = -1;
        else
            ans.n = 0;
    }
    else {
        ans.n = 1;
        ans.ans[0] = _Cdouble1(-b/a);
    }
    return ans;
}

Cans
cequation1(Cdouble a, Cdouble b)
{
    Cans ans;
    
    ans.ans[0] = ans.ans[1] = ans.ans[2] = ans.ans[3] = _C0;
    
    if (cis0(a)) {
        if (cis0(b))
            ans.n = -1;
        else
            ans.n = 0;
    }
    else {
        ans.n = 1;
        ans.ans[0] = cdiv(cneg(b), a);
    }
    return ans;
}

Cans
equation2(double a, double b, double c)
{
    Cans ans;
    
    ans.ans[0] = ans.ans[1] = ans.ans[2] = ans.ans[3] = _C0;
    
    if (a == 0.0)
        return equation1(b, c);
    
    b = -0.5 * b / a;
    c /= a;
    a = b * b - c;
    if (a >= 0.0) {
        a = sqrt(a);
        ans.ans[0] = _Cdouble1(b + a);
        ans.ans[1] = _Cdouble1(b - a);
    }
    else {
        a = sqrt(-a);
        ans.ans[0] = _Cdouble(b, a);
        ans.ans[1] = _Cdouble(b, -a);
    }
    ans.n = 2;
    return ans;
}

Cans
cequation2(Cdouble a, Cdouble b, Cdouble c)
{
    Cans ans;
    
    ans.ans[0] = ans.ans[1] = ans.ans[2] = ans.ans[3] = _C0;
    
    if (cis0(a))
        return cequation1(b, c);
    
    b = cdiv(cmul(_Cdouble1(-0.5), b), a);
    c = cdiv(c, a);
    a = Csqrt(csub(cmul(b, b), c));
    ans.ans[0] = cadd(b, a);
    ans.ans[1] = csub(b, a);
    ans.n = 2;
    return ans;
}

Cans
equation3(double a, double b, double c, double d)
{
    Cans ans;
    Cdouble w, ww, cA3;
    double A, B, C, A3;
    double p, q, p3, q2;
    
    if (a == 0.0)
        return equation2(b, c, d);
    A = b / a;
    B = c / a;
    C = d / a;
    
    w = _Cdouble(-0.5, 0.5 * sqrt(3.0));
    ww = _Cdouble(-0.5, -0.5 * sqrt(3.0));
    A3 = A / 3.0;
    cA3 = _Cdouble1(A3);
    p = B - A * A3;
    q = 2.0 * A3 * A3 * A3 - A3 * B + C;
    p3 = p / 3.0;
    q2 = -0.5 * q;
    
    d = q2 * q2 + p3 * p3 * p3;
    if (d >= 0.0) {
        d = sqrt(d);
        double m = cbrt(q2 + d);
        double n = cbrt(q2 - d);
        ans.ans[0] = _Cdouble1(m + n - A3);
        ans.ans[1] = csub(cadd(cmul(_Cdouble1(m), w), cmul(_Cdouble1(n), ww)), cA3);
        ans.ans[2] = csub(cadd(cmul(_Cdouble1(m), ww), cmul(_Cdouble1(n), w)), cA3);
    }
    else {
        d = sqrt(-d);
        Cdouble cm = ccbrt(_Cdouble(q2, d));
        Cdouble cn = ccbrt(_Cdouble(q2, -d));
        ans.ans[0] = csub(cadd(cm, cn), cA3);
        ans.ans[1] = csub(cadd(cmul(cm, w), cmul(cn, ww)), cA3);
        ans.ans[2] = csub(cadd(cmul(cm, ww), cmul(cn, w)), cA3);
    }
    ans.n = 3;
    return ans;
}

static double
Find1RealAns(Cans res)
{
    int n = res.n;
    double ii;
    int id, i;
    
    if (n <= 0) return 0.0;
    ii = fabs(res.ans[0].i);
    id = 0;
    for (i = 1; i < n; ++i) {
        double ti = fabs(res.ans[i].i);
        if (ti < ii) {
            ii = ti;
            id = i;
        }
    }
    return res.ans[id].r;
}

Cans
equation4f(double A, double B, double C)
{
    if (B == 0.0) {
        Cans res = equation2(1.0, A, C);
        Cdouble x1 = Csqrt(res.ans[0]);
        Cdouble x2 = Csqrt(res.ans[1]);
        res.ans[0] = x1;
        res.ans[1] = cneg(x1);
        res.ans[2] = x2;
        res.ans[3] = cneg(x2);
        res.n = 4;
        return res;
    }
    else {
        /* y^4 + Ay^2 + By + C =
         * (y^2+L)^2 + (A-2L)(y^2 + B/(A-2L)y + (C-L^2)/(A-2L) =
         * (y^2+L)^2 + (A-2L)[(y+B/(2(A-2L)))^2+(C-L^2)/(A-2L)-B^2/(4(A-2L)^2)) =
         * (y^2+L)^2 + (A-2L)[(y+B/(2(A-2L)))^2,
         * if (C-L^2)-B^2/(4(A-2L)) = 0.
         * (y^2+L)^2 + M(y + N)^2 = 0
         */
        Cdouble X, b1, c1, b2, c2;
        double L, M, N;
        Cans res1, res2;
        
        res1 = equation3(1.0, -0.5 * A, -C, 0.5 *  A * C - 0.125 * B * B);
        L = Find1RealAns(res1);
        M = A - L * 2.0;
        N = B / (2.0 * M);
        X = Csqrt(_Cdouble1(-M));
        b1 = cneg(X);
        c1 = csub(_Cdouble1(L), cmul(X, _Cdouble1(N)));
        b2 = X;
        c2 = cadd(_Cdouble1(L), cmul(X, _Cdouble1(N)));
        
        res1 = cequation2(_Cdouble(1.0, 0.0), b1, c1);
        res2 = cequation2(_Cdouble(1.0, 0.0), b2, c2);
        res1.ans[2] = res2.ans[0];
        res1.ans[3] = res2.ans[1];
        res1.n = 4;
        return res1;
    }
}

Cans
equation4sub(double a, double b, double c, double d)
{
    double A, B, C;
    double aa = a * a;
    Cdouble a4 = _Cdouble1(a / 4.0);
    Cans res;
    
    /* x^4 + ax^3 + bx^2 + cx + d = y^4 + Ay^2 + By + C,
     *  where x = y - a/4
     */
    
    A = b - (3.0 / 8.0) * aa;
    B = c + a * aa / 8.0 - a * b / 2.0;
    C = d - (3.0 / 256.0) * aa * aa + aa * b / 16.0 - a * c / 4.0;
    
    res = equation4f(A, B, C);
    res.ans[0] = csub(res.ans[0], a4);
    res.ans[1] = csub(res.ans[1], a4);
    res.ans[2] = csub(res.ans[2], a4);
    res.ans[3] = csub(res.ans[3], a4);
    return res;
}

Cans
equation4(double a, double b, double c, double d, double e)
{
    if (a == 0.0)
        return equation3(b, c, d, e);
    
    return equation4sub(b / a, c / a, d / a, e / a);
}



Cdouble
poly4(double a, double b, double c, double d, double e, Cdouble x)
{
    Cdouble xx = cmul(x, x);
    Cdouble xxx = cmul(xx, x);
    Cdouble xxxx = cmul(xx, xx);
    Cdouble ar = cmul(_Cdouble1(a), xxxx);
    Cdouble br = cmul(_Cdouble1(b), xxx);
    Cdouble cr = cmul(_Cdouble1(c), xx);
    Cdouble dr = cmul(_Cdouble1(d), x);
    Cdouble er = _Cdouble1(e);
    return cadd(cadd(cadd(ar, br), cadd(cr, dr)), er);
}

double getadouble(void)
{
    double x;
    
    while (scanf("%lf", &x) != 1) {
        fprintf(stderr, "Please type a number.\n");
    }
    return x;
}

