//
//  geometry.h
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/08/20.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#ifndef __UnwarpedCornea__geometry__
#define __UnwarpedCornea__geometry__

#include <iostream>
std::vector<cv::Mat> CalcIntersectionSphere2Sphere(cv::Mat &C,double rc, cv::Mat &CC, double re);
cv::Mat IntersectionRay2Sphere(cv::Mat &Ray, cv::Mat &Center, double radius);
#endif /* defined(__UnwarpedCornea__geometry__) */
