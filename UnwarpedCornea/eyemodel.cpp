//
//  eyemodel.cpp
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2015/03/13.
//  Copyright (c) 2015年 ___Kentaro Takemura___. All rights reserved.
//

#include <opencv2/opencv.hpp>
#include <boost/asio.hpp>
#include <GLFW/glfw3.h>
#include <OpenGL/glu.h>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <iostream>
#include <iomanip>
#include <ctype.h>

#include "axis.h"
#include "eyeball.h"
#include "geometry.h"
#include "image.h"
#include "equation.h"
#include "common.h"
#include "sharedmem.h"
#include "eyemodel.h"
#include "eyemodel.h"

extern double focus;
extern double d;
static const GLfloat aLight0pos[] = { 0.0, 3.0, 5.0, 1.0 };// 光源0の位置。
static const GLfloat aLight1pos[] = { 5.0, 3.0, 0.0, 1.0 };// 光源1の位置。
extern double MMPERPIXEL;
extern cv::Mat img;
extern cv::Mat LrefPos;
extern double max_pitch;
extern double max_yaw;
extern std::vector<cv::Point3f> illumination_3;
extern std::vector<cv::Point2f> transformation;
extern std::vector<cv::Mat> intersection;
extern std::vector<cv::Mat> nv1;
extern cv::Mat C;



//OpenGL用の色指定

double black[]={0,0,0};
double white[]={1,1,1};
double red[]={1,0,0};
double green[]={0,1,0};
double blue[]={0,0,1};
double zero[]={0,0,0};
double brown[]={0.4,0.2,0.1};
extern GLFWwindow *glwindow;



void showModel(){
    std::cout << "-----------------Show Model------------------------"<<std::endl;
    
    //Draw the eye model in OpenGL. 眼球モデルの描画
    //GLFWwindow* glwindow=initalize();
       glfwMakeContextCurrent(glwindow);
    while (!glfwWindowShouldClose(glwindow)){
        if(max_pitch!=0 && max_yaw!=0){
        glScalef (-1., 1., 1.);//Convert to right hand model. OpenGLに右手系を表示するトリック
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );// Clear display.
        glLoadIdentity();
    
        
        int width, height;
        glfwGetFramebufferSize(glwindow, &width, &height);
        glViewport( 0, 0, width, height );    //viewport
        gluPerspective( 30.0, (double)width / (double)height, 1.0, 5000.0 );// 透視投影
        gluLookAt( 50.0, -50.0, -100.0,  0.0, 0.0, focus+d,  0.0, -1.0, 0.0 );    // 視点の設定
        //        gluLookAt( -300.0, 0.0, 0.0,  0.0, 0.0, 0,  0.0, -1.0, 0.0 );    // 視点の設定
        glLightfv( GL_LIGHT0, GL_POSITION, aLight0pos );    // 光源の位置設定 （＊重要 視点の位置を設定した後に行う）
        glLightfv( GL_LIGHT1, GL_POSITION, aLight1pos );
        
        glViewport(0, 0, width, height);
        double image_plane_pos[]={0,0,focus};
        drawAxisXYZ(zero);
        drawGridPlaneXZ();
        drawImagePlane(green,img.cols*MMPERPIXEL,img.rows*MMPERPIXEL,image_plane_pos);
        showEyeModel(LrefPos,max_pitch,max_yaw);
//           for(int i = 0;i<3; i++){
           
            
         
            for(int i=0 ; i<3 ; i++){
                glBegin(GL_LINES);
                glVertex3f(0,0,0);//カメラからrayの衝突点までのベクトル
                glVertex3f(intersection[i].at<double>(0),intersection[i].at<double>(1),intersection[i].at<double>(2));
                glEnd();
                
            glBegin(GL_LINES);
            glColor3f(0,1,0);
//            glVertex3f(intersection.at<double>(0),intersection.at<double>(1),intersection.at<double>(2));//衝突点からの法線ベクトル}
            glVertex3f(C.at<double>(0),C.at<double>(1),C.at<double>(2));
            glVertex3f(nv1[i].at<double>(0)*20+intersection[i].at<double>(0),nv1[i].at<double>(1)*20+intersection[i].at<double>(1),nv1[i].at<double>(2)*20+intersection[i].at<double>(2));//衝突点をたすs
           glEnd();
            }
            
//            std::cout<< "nv1x" <<nv1[0].at<double>(0)*10+intersection.at<double>(0)<<std::endl;
//            std::cout<< "nv1y" <<nv1[0].at<double>(1)*10+intersection.at<double>(1)<<std::endl;
//            std::cout<< "nv1z" <<nv1[0].at<double>(2)*10+intersection.at<double>(2)<<std::endl;
            
//            drawSphere(intersection.ptr<double>(0),0.3 , red);


//           }

            
#ifdef CELLING
        drawSphere(start.ptr<double>(0),1.0 , red);
        drawSphere(goal.ptr<double>(0),1.0 , red);
            
#endif
        //        drawTangentPlane(plane);
        //        drawTangentPlane(Point);
        
        glfwSwapBuffers(glwindow);
//        glfwPollEvents();
        }
    }
}
