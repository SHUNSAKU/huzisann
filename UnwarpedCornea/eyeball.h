//
//  eyeball.h
//  UnwarpedCornea
//
//  Created by Kentaro Takemura on 2014/08/19.
//  Copyright (c) 2014年 ___Kentaro Takemura___. All rights reserved.
//

#ifndef __UnwarpedCornea__eyeball__
#define __UnwarpedCornea__eyeball__

#include <iostream>
void set_eye_paramter();
void CreateEyeModel(cv::RotatedRect &box);
void CalcEyeAxis(cv::Point2f &left, cv::Point2f &right);
std::vector<cv::Mat> CalcLimbus();
void showEyeModel(cv::Mat &LrefPos,double pitch, double yaw);
std::vector<cv::Mat> rotateEyeModel(double pitch, double yaw, std::vector<cv::Mat> iris3);
cv::Mat EstimateIrisCenterOnCornea();
cv::Mat SetPlane(cv::Mat &LrefPos);
cv::Mat CalcurateReflectionVec(cv::Mat &plane);
#endif /* defined(__UnwarpedCornea__eyeball__) */
